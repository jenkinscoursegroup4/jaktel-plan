package com.jakty.jaktel.plan.repository;


import com.jakty.jaktel.plan.entity.Plan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanRepository extends JpaRepository<Plan, Integer> {
	


}
